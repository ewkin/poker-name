const suits = {
    DIAMONDS : 'd',
    HEARTS : 'h',
    CLUBS : 'c',
    SPADES : 's'
};

const ranks = {
    TWO: '2',
    THREE: '3',
    FOUR: '4',
    FIVE: '5',
    SIX: '6',
    SEVEN: '7',
    EIGHT: '8',
    NINE: '9',
    TEN: '10',
    JACK: 'J',
    QUEEN: 'Q',
    KING: 'K',
    ACE: 'A'
};

class CardDeck {
    cards = [];

    constructor(back) {
        for(let suit in suits){
             for(let rank in ranks){
                 this.cards.push({
                     suit: suits[suit],
                     rank: ranks[rank],
                     back: back
                 });
             }
        }

    }

    getCard(){
        let randomIndexCard = Math.floor(Math.random()*this.cards.length);
        let [card] = this.cards.splice(randomIndexCard, 1);
        return card;
    }

    getCards(quantOfCards){
        const cards = [];
        for (let i =0; i < quantOfCards; i++){
            cards.push(this.getCard());
        }
        return cards;

    }


}


export default CardDeck;