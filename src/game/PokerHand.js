class PokerHand {
    constructor(hand) {
        this.hand = hand;
    }

    evaluateHand = () => {
        let ranks = [];
        let suits = [];

        this.hand.map((val, i) =>{
            switch(val.suit) {
                case 'c':
                    suits.push(1);
                    break;

                case 'd':
                    suits.push(2);
                    break;
                case 'h':
                    suits.push(4);
                    break;
                case 's':
                    suits.push(8);
                    break;
            }
            switch (val.rank){
                case 'J':
                    ranks.push(11);
                    break;
                case 'Q':
                    ranks.push(12);
                    break;
                case 'K':
                    ranks.push(13);
                    break;
                case 'A':
                    ranks.push(14)
                    break;
                default:
                    ranks.push(parseInt(val.rank))
            }
        });
        return this.evaluationAlgorithm(ranks, suits);
    }

    evaluationAlgorithm = (cs, ss) => {
        let pokerHands = ["4 of a Kind", "Straight Flush","Straight","Flush","High Card","1 Pair","2 Pair","Royal Flush", "3 of a Kind","Full House"];

        let v,i,o,s = 1 << cs[0] | 1 << cs[1] | 1 << cs[2] | 1 << cs[3] | 1 << cs[4];
        for (i = -1, v = o = 0; i < 5; i++, o = Math.pow(2, cs[i] * 4)) {v += o * ((v / o & 15) + 1);}
        v = v % 15 - ((s / (s & -s) === 31) || (s === 0x403c) ? 3 : 1);
        v -= (ss[0] === (ss[1] | ss[2] | ss[3] | ss[4])) * ((s === 0x7c00) ? -5 : 1);

        return pokerHands[v];

        // link to the master code: https://www.codeproject.com/Articles/569271/A-Poker-hand-analyzer-in-JavaScript-using-bit-math
    }
}

export default PokerHand