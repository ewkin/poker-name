import React from "react";
import './App.css';
import Card from "./components/Card/Card";
import CardDeck from "./game/CardDeck";
import PokerHand from "./game/PokerHand";




class App extends React.Component {



  state = {
    card: new CardDeck(true).getCards(5),
    result: [{text: '', show: false}],
  };

  getRandomCards = () => {
      let cards = new CardDeck(false).getCards(5);

      const card = this.state.card.map((val, i) =>{
          return {
              suit: cards[i].suit,
              rank: cards[i].rank,
              back: cards[i].back
          };
      });
      this.setState({card});
      const result = this.state.result.map((val, i) =>{
          return {
              text: '',
              show: false
          };
      });
      this.setState({result});
  };

  openCard = (index) =>{
      const card =[...this.state.card];
      const oneCard={...card[index]};
      oneCard.back = false;
      card[index]=oneCard;
      this.setState({card});
  };

  showResult = () =>{
    let results = new PokerHand(this.state.card);
    let resultText = results.evaluateHand();

    const result = this.state.result.map((val, i) =>{
          return {
              text: resultText,
              show: true
          };
      });
      this.setState({result});

  };

  render() {
    return (
        <div className="App">
            <button onClick={this.getRandomCards}>
                New cards
            </button>
            <div>
                <div onClick={e => this.openCard(0)}  style = {{display: "inline-block"}}>
                    <Card suit = {this.state.card[0].suit} back = {this.state.card[0].back} rank = {this.state.card[0].rank}/>
                </div>
                <div onClick={e => this.openCard(1)} style = {{display: "inline-block"}}>
                    <Card suit = {this.state.card[1].suit} back = {this.state.card[1].back} rank = {this.state.card[1].rank}/>
                </div>
                <div onClick={e => this.openCard(2)} style = {{display: "inline-block"}}>
                    <Card suit = {this.state.card[2].suit} back = {this.state.card[2].back} rank = {this.state.card[2].rank}/>
                </div>
                <div onClick={e => this.openCard(3)} style = {{display: "inline-block"}}>
                    <Card suit = {this.state.card[3].suit} back = {this.state.card[3].back} rank = {this.state.card[3].rank}/>
                </div>
                <div onClick={e => this.openCard(4)} style = {{display: "inline-block"}}>
                    <Card suit = {this.state.card[4].suit} back = {this.state.card[4].back} rank = {this.state.card[4].rank}/>
                </div>
              </div>
            <button onClick={this.showResult}>
                Show result
            </button>
            {this.state.result[0].show ? <div> {this.state.result[0].text} </div> : null}

        </div>
    );
  }
}

export default App;
