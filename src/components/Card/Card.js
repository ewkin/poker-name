import React from "react";
import "./Card.css"


const suits = {
    h:{className: 'Card-hearts', symbol: '♥'},
    c:{className: 'Card-clubs', symbol: '♣'},
    d:{className: 'Card-diams', symbol: '♦'},
    s:{className: 'Card-spades', symbol: '♠'}
};

const Card = props => {
    const suitClass = suits[props.suit].className;
    const symbol = suits[props.suit].symbol;

    const cardClasses = [
        'Card',
        'Card-rank-' + props.rank.toLowerCase(),
        suitClass
    ];

    if(props.back){
        cardClasses.push('Card-back')
    }

    return (
        <div className = {cardClasses.join(' ')}>
            <span className="Card-rank">{props.rank}</span>
            <span className="Card-suit">{symbol}</span>
        </div>
    )

}


export default Card;